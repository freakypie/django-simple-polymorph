Intended to be a super simple way for you to polymorph your django models

if you create a model like this:

    from simple_polymorph import PolyMorph

    class Location(PolyMorph):
        index = models.IntegerField()


and subclass it like this:

    class Basement(Location):
        dark = models.BooleanField(default=True)

you can get the real type of an object like this:

    Location.objects.get().polymorph()

    >>> <Basement object>

This will cost a database query per polymorph and has no ability to run in bulk.
If you need that or more advanced operations, try "django-polymorphic"
