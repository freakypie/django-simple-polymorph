#!/usr/bin/env python
from setuptools import setup, find_packages

setup(
    name='django-simple-polymorph',
    version='0.3',
    description="A simple utility to polymorph inherited models",
    author="Danemco, LLC",
    author_email='dev@velocitywebworks.com',
    url='https://bitbucket.org/freakypie/django-simple-polymorph',
    packages=find_packages(),
    install_requires=[],
)
